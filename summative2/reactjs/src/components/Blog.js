import React, { Component } from "react";
import ImgDolphins from "../assets/images/dolphins.jpg";
import BlogContent from "./BlogContent";
import ImgGorilla from "../assets/images/gorilla-2.jpg";
import ImgSnake from "../assets/images/snake-2.jpg";
import ImgButterfly from "../assets/images/butterfly-2.jpg";
import axios from "axios";

export default class Blog extends Component {
  constructor() {
    super();
    this.state = {
      blogContent: [],
    };
  }

  componentDidMount() {
    this.getBlogContent();
  }

  async getBlogContent() {
    const fetchData = await axios
      .get("http://localhost:8080/blog/listAll")
      .then((res) => res.data)
      .then((data) => {
        this.setState({
          blogContent: data,
        });
      });
  }
  render() {
    const size = this.state.blogContent.length;
    const sizeBlogContent1 = Math.ceil(size / 2);

    const blogContent1 = this.state.blogContent
      .slice(0, sizeBlogContent1)
      .map((data) => {
        return (
          <BlogContent
            imgUrl={data.imgUrl}
            title={data.title}
            content={data.content}
          />
        );
      });

    const blogContent2 = this.state.blogContent
      .slice(sizeBlogContent1, size)
      .map((data) => {
        return (
          <BlogContent
            imgUrl={data.imgUrl}
            title={data.title}
            content={data.content}
          />
        );
      });
    return (
      <div class="section2">
        <h2>Blog : Curabitur sodales</h2>
        <p>This website template has been designed by Nexsoft Bootcamp</p>
        <a href="#">
          <img src={ImgDolphins} alt="" />
        </a>
        <ul>
          <li>
            <p>Lorem ipsum dolor sit amet.</p>
          </li>
        </ul>
        <div id="section1">
          <ul>{blogContent1}</ul>
        </div>
        <div id="section2">
          <ul>{blogContent2}</ul>
        </div>
      </div>
    );
  }
}
