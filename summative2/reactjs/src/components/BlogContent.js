import React from "react";

export default function BlogContent(props) {
  return (
    <li>
      <a href="#">
        <img src={props.imgUrl} alt="" />
      </a>
      <h4>
        <a href="#">{props.title}</a>
      </h4>
      <p>{props.content}</p>
    </li>
  );
}
