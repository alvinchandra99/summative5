import React from "react";
import SocialMedia from "./SocialMedia";
import ImgPenguin from "../assets/images/penguin2.jpg";

export default function Connect() {
  return (
    <div class="section3">
      <h2>Connect</h2>
      <SocialMedia id="email" name="Email Us" />
      <SocialMedia id="facebook" name="Facebook" />
      <SocialMedia id="twitter" name="Twitter" />
      <form action="#">
        <h3>Subscribe to our</h3>
        <h2>NEWSLETTER</h2>
        <input type="text" value="your email here..." />
      </form>
      <img src={ImgPenguin} alt="" />
    </div>
  );
}
