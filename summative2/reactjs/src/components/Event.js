import React, { Component } from "react";
import axios from "axios";
import EventItem from "./EventItem";

export default class Event extends Component {
  constructor() {
    super();
    this.state = {
      eventItem: [],
    };
  }

  componentDidMount() {
    this.getEventItem();
  }

  async getEventItem() {
    const fetchData = await axios
      .get("http://localhost:8080/event/listAll")
      .then((res) => res.data)
      .then((data) => {
        this.setState({
          eventItem: data,
        });
      });
  }

  render() {
    let i = 0;
    const eventItem = this.state.eventItem.map((data) => {
      let cssClass = "";
      if (i == 0) {
        cssClass = "first";
      }
      i++;
      return (
        <EventItem
          key={data.id}
          class={cssClass}
          date={data.date}
          content={data.event}
        />
      );
    });
    return (
      <div class="section1">
        <h2>Events</h2>
        <ul id="article">
          {eventItem}
        </ul>
      </div>
    );
  }
}
