import React from "react";

export default function EventItem(props) {
  return (
    <li class={props.class}>
      <a href="#">
        <span>{props.date}</span>
      </a>
      <p>{props.content}</p>
    </li>
  );
}
