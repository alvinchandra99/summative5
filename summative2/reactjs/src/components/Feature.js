import React, { Component } from "react";
import axios from "axios";
import ImageGallery from "./ImageGallery";


export default class Feature extends Component {
  constructor() {
    super();
    this.state = {
      imgGallery: [],
    };
  }

  componentDidMount() {
    this.getImageGallery();
  }

  async getImageGallery() {
    const fetchData = await axios
      .get("http://localhost:8080/animal/listAll")
      .then((res) => res.data)
      .then((data) =>
        this.setState({
          imgGallery: data,
        })
      )
      .catch((error) => console.log(error));
  }

  render() {
    let i = 0;
    const imgGallery = this.state.imgGallery.map((data) => {
      let cssClass = "";
      if (i == 0) {
        cssClass = "first";
      }
      if (i == this.state.imgGallery.length - 1) {
        cssClass = "last";
      }
      i = i + 1;
      return (
        <ImageGallery
          key={data.id}
          imgUrl={data.imgUrl}
          name={data.name}
          class={cssClass}
        />
      );
    });
    return (
      <div id="featured">
        <h2>Meet our Animals</h2>
        <ul>{imgGallery}</ul>
      </div>
    );
  }
}
