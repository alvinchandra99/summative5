import React from "react";

export default function First(props) {
  return (
    <li class={props.class}>
      <h2>
        <a href="">{props.title}</a>
      </h2>
      <span>{props.content}</span>
    </li>
  );
}
