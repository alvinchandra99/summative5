import React from "react";

export default function FooterContent(props) {
  return (
    <li>
      <a href="#">{props.content}</a>
    </li>
  );
}
