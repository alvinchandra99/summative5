import React from "react";
import img from "../assets/images/lion-family.jpg";

export default function HeaderImage() {
  return <img src={img} alt="" />;
}
