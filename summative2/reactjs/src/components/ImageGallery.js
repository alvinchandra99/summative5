import React from "react";

export default function ImageGallery(props) {
  return (
    <li class={props.class}>
      <a href="#">
        <img src={props.imgUrl} alt="" />
      </a>
      <a href="#">{props.name}</a>
    </li>
  );
}
