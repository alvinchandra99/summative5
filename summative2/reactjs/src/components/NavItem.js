import React from "react";

export default function NavItem(props) {
  return (
    <li id={props.id} class={props.class}>
      <a href="">{props.content}</a>
    </li>
  );
}
