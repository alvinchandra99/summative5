import React from "react";
import NavItem from "./NavItem";

export default function Navigation() {
  return (
    <ul id="navigation">
      <NavItem class="selected" id="link1" content="Home" />
      <NavItem id="link2" content="The Zoo" />
      <NavItem id="link3" content="Visitors Info" />
      <NavItem id="link4" content="Tickets" />
      <NavItem id="link5" content="Events" />
      <NavItem id="link6" content="Galery" />
      <NavItem id="link6" content="Contact Us" />
    </ul>
  );
}
