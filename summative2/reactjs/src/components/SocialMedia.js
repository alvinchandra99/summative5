import React from "react";

export default function SocialMedia(props) {
  return (
    <a href="#" id={props.id}>
      {props.name}
    </a>
  );
}
