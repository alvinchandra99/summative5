import React from "react";
import Feature from "../components/Feature";
import Event from "../components/Event";
import Blog from "../components/Blog";
import Connect from "../components/Connect";
export default function Content() {
  return (
    <div id="content">
      <Feature />
      <Event />
      <Blog />
      <Connect />
    </div>
  );
}
