import React, { Component } from "react";
import Copyright from "../components/Copyright";
import FooterContent from "../components/FooterContent";
import NavItem from "../components/NavItem";

export default class Footer extends Component {
  render() {
    return (
      <div id="footer">
        <div>
          <a href="#" class="logo">
            <img src="images/animal-kingdom.jpg" alt="" />
          </a>
          <div>
            <p>
              Lorem ipsum dolor sit, amet consectetur adipisicing elit. Nulla,
              libero!
            </p>
            <span>08 880 008 880</span>
            <span>loremipsum@gmail.com</span>
          </div>
          <ul class="navigation">
            <NavItem content="Home" />
            <NavItem content="Tickets" />
            <NavItem content="The Zoo" />
            <NavItem content="Events" />
            <NavItem content="Blog" />
            <NavItem content="Galery" />
          </ul>
          <ul>
            <FooterContent content="Live : Have fun in your visit" />
            <FooterContent content="Love : Donate for the animals" />
            <FooterContent content="Learn : Get to know the animals" />
          </ul>
          <Copyright />
        </div>
      </div>
    );
  }
}
