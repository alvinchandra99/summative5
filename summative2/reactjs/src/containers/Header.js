import React, { Component } from "react";
import Button from "../components/Button";
import First from "../components/First";
import HeaderImage from "../components/HeaderImage";
import Logo from "../components/Logo";
import Navigation from "../components/Navigation";
import SpecialEvent from "../components/SpecialEvent";

export default class Header extends Component {
  render() {
    return (
      <div id="header">
        <Logo />
        <ul>
          <First class="first" title="Live" content="Have fun in your visit" />
          <First title="Love" content="Donate for the animals" />
          <First title="Learn" content="Get to know animals" />
        </ul>
        <Button content="Buy tickets / Check Events" />
        <Navigation />
        <HeaderImage />
        <SpecialEvent
          title="Special Events"
          content="This website template has been designed by Nexsoft BootCamp"
        />
      </div>
    );
  }
}
