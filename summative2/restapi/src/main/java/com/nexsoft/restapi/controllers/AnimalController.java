package com.nexsoft.restapi.controllers;

import java.util.List;

import com.nexsoft.restapi.models.Animal;
import com.nexsoft.restapi.service.AnimalService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("http://localhost:3000")
@RequestMapping("/animal")
public class AnimalController {
    
    @Autowired
    AnimalService animalService;

    @GetMapping("/listAll")
    public List<Animal> listAll(){
        return animalService.findAll();
    }

}
