package com.nexsoft.restapi.controllers;

import java.util.List;

import com.nexsoft.restapi.models.Blog;
import com.nexsoft.restapi.service.BlogService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("http://localhost:3000")
@RequestMapping("/blog")
public class BlogController {

    @Autowired
    BlogService blogService;

    @GetMapping("/listAll")
    public List<Blog> listAll(){
        return blogService.listAll();
    }

}
