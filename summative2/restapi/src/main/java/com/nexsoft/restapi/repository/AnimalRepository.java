package com.nexsoft.restapi.repository;

import java.util.List;

import com.nexsoft.restapi.models.Animal;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnimalRepository extends CrudRepository<Animal,Integer>{

    public List<Animal> findAll();
    
}
