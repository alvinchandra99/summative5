package com.nexsoft.restapi.repository;

import java.util.List;

import com.nexsoft.restapi.models.Blog;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BlogRepository extends CrudRepository<Blog, Integer> {
    
    public List<Blog> findAll();
}
