package com.nexsoft.restapi.repository;

import java.util.List;

import com.nexsoft.restapi.models.Event;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface EventRepository extends CrudRepository<Event, Integer>{
   
    public List<Event> findAll();

}
