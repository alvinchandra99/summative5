package com.nexsoft.restapi.service;

import java.util.List;

import javax.transaction.TransactionScoped;

import com.nexsoft.restapi.models.Animal;
import com.nexsoft.restapi.repository.AnimalRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@TransactionScoped
public class AnimalService {

    @Autowired
    AnimalRepository animalRepository;
    
    public List<Animal> findAll(){
        return animalRepository.findAll();
    }
}
