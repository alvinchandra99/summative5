package com.nexsoft.restapi.service;

import java.util.List;

import javax.transaction.TransactionScoped;

import com.nexsoft.restapi.models.Blog;
import com.nexsoft.restapi.repository.BlogRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@TransactionScoped
public class BlogService {

    @Autowired
    BlogRepository blogRepository;

    public List<Blog> listAll(){
        return blogRepository.findAll();
    }
}
