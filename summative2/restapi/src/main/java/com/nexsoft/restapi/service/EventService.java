package com.nexsoft.restapi.service;

import java.util.List;

import javax.transaction.TransactionScoped;

import com.nexsoft.restapi.models.Event;
import com.nexsoft.restapi.repository.EventRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@TransactionScoped
public class EventService {

    @Autowired
    EventRepository eventRepository;

    public List<Event> findAll(){
        return eventRepository.findAll();
    }
    
}
